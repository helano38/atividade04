import cv2
import numpy as np
import sys
import RLE as compressor

cap = cv2.VideoCapture('pdi.mp4')
k = 0
while True:
    _,frame = cap.read()
    if frame is None:
        break
    #cv2.imshow('app', frame)
    #cv2.imwrite('./IMG/' + str(k) + '.png', frame,  [cv2.IMWRITE_PNG_COMPRESSION, 9])
    gray = cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2GRAY)
    frame = compressor.encodeImage(gray, gray.shape[0], gray.shape[1], '1', "C" )
    compsize, filepath = compressor.saveCompressedToFile(frame, "img"+str(k))
    print(gray)
    #cv2.imwrite('./IMG/' + str(k) + '.png', frame)

    k = k + 1

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
